package devcunquero.com.eatgopos.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;

import java.util.Objects;

import devcunquero.com.eatgopos.Callback.ICloseDialog;
import devcunquero.com.eatgopos.Fragment.ChargeFragment;
import devcunquero.com.eatgopos.Fragment.HistoryFragment;
import devcunquero.com.eatgopos.Fragment.ScanFragment;
import devcunquero.com.eatgopos.Model.User;
import devcunquero.com.eatgopos.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ICloseDialog {

    private SharedPreferences sharedPreferences;
    private ICloseDialog iCloseDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        CoordinatorLayout CSnakbarLayout;

        CSnakbarLayout = (CoordinatorLayout) findViewById(R.id.containerMain);

        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

       sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE);
        User.id_user = sharedPreferences.getString("id_user", "");
        User.username = sharedPreferences.getString("username", "");


        if(User.id_user.equals("")){
            startActivity(new Intent(this, LoginActivity.class));
        }

        this.setFragment(new ChargeFragment());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        this.iCloseDialog.onCloseDialog();

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.iCloseDialog = (ICloseDialog) this;

    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.iCloseDialog = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_scan) {

            this.setFragment(new ChargeFragment());

        } else if (id == R.id.nav_history) {

            this.setFragment(new HistoryFragment());
        } else if (id == R.id.nav_close) {
            this.closeSession();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void closeSession() {
        SharedPreferences settings = getSharedPreferences("session", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("username");
        editor.remove("id_user");
        editor.commit();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.containerFragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();



    }



    @Override
    public void onCloseDialog() {
        ScanFragment frag = (ScanFragment)
                getSupportFragmentManager().findFragmentById(R.id.containerFragment);
        frag.onCloseDialog();
    }
    
  

}
