package devcunquero.com.eatgopos.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

import devcunquero.com.eatgopos.R;
import devcunquero.com.eatgopos.Util.Constant;
import devcunquero.com.eatgopos.Util.KeyBoard;
import devcunquero.com.eatgopos.Util.Progress;
import devcunquero.com.eatgopos.Util.SnackBar;

public class LoginActivity extends AppCompatActivity {


    private Button btnLogin;
    private EditText editUsername , editPassword;
    private SharedPreferences sharedPreferences;
    private CoordinatorLayout CSnakbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        CSnakbarLayout = (CoordinatorLayout) findViewById(R.id.containerMain);
        btnLogin = findViewById(R.id.btnLogin);
        editPassword = findViewById(R.id.editPassword);
        editUsername = findViewById(R.id.editUsername);



        sharedPreferences = getSharedPreferences("session",Context.MODE_PRIVATE);
        if(!sharedPreferences.getString("id_user", "").equals("")){
            startActivity(new Intent(this, MainActivity.class));
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyBoard.hide(LoginActivity.this,editPassword);
                if(editPassword.getText().toString().length() == 0 || editUsername.getText().toString().length()  == 0){
                    new SnackBar().error(LoginActivity.this,"Los campos son obligatorios", CSnakbarLayout);
                }else{
                    btnLogin.setEnabled(false);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("username", editUsername.getText().toString());
                        jsonObject.put("password", editPassword.getText().toString());

                        sendRequest(jsonObject, "sp_selAuthenticationPos");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public void onSuccess(JSONObject jsonObject) {
        JSONArray data = null;
        try {
            data = jsonObject.getJSONArray("data");

            if (data.length() > 0) {
                String username = data.getJSONObject(0).getString("username");
                String id_user = data.getJSONObject(0).getString("id_user");
                sharedPreferences.edit().putString("username",username).apply();
                sharedPreferences.edit().putString("id_user",id_user).apply();

                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);

            } else {
                new SnackBar().error(this, "Usuario / contrseña incorrecta",CSnakbarLayout);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void onError(VolleyError error) {
        new SnackBar().error(this, "Se ha producido un error", CSnakbarLayout);
    }

    private void sendRequest(JSONObject model, String sp) {
        try {
            Progress.showLoading((Objects.requireNonNull(this)));
            RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(this));

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("sp", sp);
            jsonBody.put("model", model);
            final String requestBody = jsonBody.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL_BASE + "execute", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Progress.hideLoading();
                        btnLogin.setEnabled(true);
                        onSuccess(new JSONObject(response));
                        //    closeDialog();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Progress.hideLoading();
                    btnLogin.setEnabled(true);
                    onError(error);
                    new SnackBar().error(LoginActivity.this, error.getMessage(), CSnakbarLayout);
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
