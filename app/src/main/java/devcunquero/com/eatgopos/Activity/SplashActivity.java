package devcunquero.com.eatgopos.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import devcunquero.com.eatgopos.R;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1500;
    private View img_logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        img_logo = findViewById(R.id.img_logo);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this,LoginActivity.class);

                // create the transition animation - the images in the layouts
                // of both activities are defined with android:transitionName="robot"

                // start the new activity
                startActivity(mainIntent);
                finish();


            }
        }, SPLASH_DISPLAY_LENGTH);


        ImageView myImageView= (ImageView)findViewById(R.id.img_logo);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade);
        myImageView.startAnimation(myFadeInAnimation); //Set animation to your ImageView
    }
}
