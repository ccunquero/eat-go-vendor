package devcunquero.com.eatgopos.Util;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import devcunquero.com.eatgopos.R;

public class SnackBar {
    private int duration = 4000;
    public  void success(Context context, String message, CoordinatorLayout layout){

        final TSnackbar snackbar = TSnackbar.make(layout, message, TSnackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    snackbar.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, duration);

        View snackbarView = snackbar.getView();
        ViewGroup.LayoutParams params = snackbarView.getLayoutParams();
        snackbarView.setLayoutParams(params);
        snackbarView.setBackgroundColor(context.getResources().getColor(R.color.colorSuccess));
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(1);
        snackbar.show();
    }

    public void error(Context context, String message, CoordinatorLayout layout){
        if(message == null) message = "Se ha producido un error";
        final TSnackbar snackbar = TSnackbar.make(layout, message, TSnackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(Color.WHITE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    snackbar.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, duration);

        View snackbarView = snackbar.getView();
        ViewGroup.LayoutParams params = snackbarView.getLayoutParams();
        snackbarView.setLayoutParams(params);
        snackbarView.setBackgroundColor(context.getResources().getColor(R.color.colorError));
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(1);
        snackbar.show();
    }
}
