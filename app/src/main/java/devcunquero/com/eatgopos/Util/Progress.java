package devcunquero.com.eatgopos.Util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.util.Objects;

import devcunquero.com.eatgopos.R;
import ir.alirezabdn.wp7progress.WP10ProgressBar;

public class Progress {
    private static AlertDialog alertDialog;


    private static WP10ProgressBar progressBar;

    public static void showLoading(Activity activity) {
        View view = activity.getLayoutInflater().inflate(R.layout.loading, null, false);
         progressBar = view.findViewById(R.id.progressBar);


             alertDialog = new AlertDialog.Builder(activity)
                     .setCancelable(false)
                     .setView(view)

                     .show();
             progressBar.showProgressBar();
             Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

             alertDialog.show();



    }

    public static void hideLoading() {
        progressBar.hideProgressBar();
        alertDialog.setCancelable(true);
        alertDialog.hide();
        alertDialog.cancel();

    }

}
