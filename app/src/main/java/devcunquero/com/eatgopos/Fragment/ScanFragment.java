package devcunquero.com.eatgopos.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

import devcunquero.com.eatgopos.Callback.ICloseDialog;
import devcunquero.com.eatgopos.R;
import devcunquero.com.eatgopos.Util.Constant;
import devcunquero.com.eatgopos.Util.Progress;
import devcunquero.com.eatgopos.Util.SnackBar;
import ir.alirezabdn.wp7progress.WP10ProgressBar;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ScanFragment extends Fragment implements ZXingScannerView.ResultHandler,
        ICloseDialog {


    private ZXingScannerView mScannerView;
    private String user_type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scan, container, false);
        // Inflate the layout for this fragment
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity(), "The context can´t be null"),
                new String[]{Manifest.permission.CAMERA},
                1);


        ViewGroup contentFrame = view.findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(getActivity());
        contentFrame.addView(mScannerView);


        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_right);
                    transaction.replace(R.id.containerFragment, new ChargeFragment());
                    transaction.addToBackStack(null);
                    transaction.commit();
                    return true;
                }
                return false;
            }
        });

    }


    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(final Result rawResult) {

        try {

            String[] data = rawResult.getText().split(":");
            String id = data[1];
            String type = data[0];
            //  this.getData(id, type);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("id", id);
            jsonObject.put("type", type);
            this.user_type = type;
            this.sendRequest(jsonObject, "sp_getUserByType");


        } catch (Exception ex) {
            new SnackBar().error(getContext(), "EL QR no es valido", (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
            mScannerView.resumeCameraPreview(this);
        }


        //

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {

                }
                return;
            }
        }
    }

    public void setFragment(Fragment fragment, Bundle bundle) {

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(android.R.id.content, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onCloseDialog() {
        mScannerView.resumeCameraPreview(this);
    }


    public void onSuccess(JSONObject jsonObject) {
        JSONArray data = null;
        try {
            data = jsonObject.getJSONArray("data");
            if (data.length() != 0) {
                String username = data.getJSONObject(0).getString("username");
                String id = data.getJSONObject(0).getString("id");
                String available_money = data.getJSONObject(0).getString("available_money");
                Bundle bundle = new Bundle();
                bundle.putString("USERNAME", username);
                bundle.putString("ID", id);
                bundle.putString("USER_TYPE", this.user_type);
                bundle.putString("AVAILABLE_MONEY", available_money);

                this.setFragment(new AmountFragment(), bundle);
            } else {
                new SnackBar().error(getContext(), "Este usuario no existe", (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
                mScannerView.resumeCameraPreview(this);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

    }

    private void sendRequest(JSONObject model, String sp) {
        try {
            Progress.showLoading(getActivity());
            RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("sp", sp);
            jsonBody.put("model", model);
            final String requestBody = jsonBody.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL_BASE + "execute", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Progress.hideLoading();
                        onSuccess(new JSONObject(response));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Progress.hideLoading();
                    ScanFragment.this.onErrorResponse(error);
                    new SnackBar().error(getContext(), "Error al obtener datos", (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
