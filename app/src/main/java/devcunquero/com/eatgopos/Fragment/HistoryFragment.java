package devcunquero.com.eatgopos.Fragment;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import devcunquero.com.eatgopos.Adapter.RecyclerAdapterHistory;
import devcunquero.com.eatgopos.Callback.IListenerHistory;
import devcunquero.com.eatgopos.Model.History;
import devcunquero.com.eatgopos.Model.User;
import devcunquero.com.eatgopos.R;
import devcunquero.com.eatgopos.Util.Constant;
import devcunquero.com.eatgopos.Util.Progress;
import devcunquero.com.eatgopos.Util.SnackBar;

public class HistoryFragment extends Fragment {

    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private View view;
    private RecyclerAdapterHistory adapterHistory;
    private SwipeRefreshLayout swipeLayout;
    private String currentFilter ;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = getLayoutInflater().inflate(R.layout.fragment_history, container, false);
        ImageView refresh = view.findViewById(R.id.imgRefresh);

        setHasOptionsMenu(true);
        this.initSpinner(view);
        this.getData("TODAY", false);
        this.currentFilter = "TODAY";


        swipeLayout = view.findViewById(R.id.swipeContainer);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(currentFilter, true);

            }
        });

        // Scheme colors for animation
        swipeLayout.setColorSchemeColors(
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark)

        );
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData(currentFilter, false);
            }
        });


        return view;
    }

    private void initRecycler(View view, List<History> objects) {

        recycler = view.findViewById(R.id.recycler_history);
        layoutManager = new LinearLayoutManager(getActivity());

        adapterHistory = new RecyclerAdapterHistory(getActivity(), objects, new IListenerHistory() {

            @Override
            public void onItemDelete(final History object, int position) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Reversar Monto");
                builder.setMessage("¿Desea reversar el monto? \n \n" + "Usuario : " + object.getUsername() + "\nMonto : " + object.getAmount() );


                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        JSONObject model = new JSONObject();
                        try {
                            model.put("id_transaction", object.getId());
                            model.put("id_user_delete", User.id_user);
                            model.put("id_user", object.getId_user());
                            model.put("type", object.getUser_type());


                            sendRequestDelete(model, "sp_delTransactionQR");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.show();

            }
        });


        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapterHistory);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Buscar...");
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapterHistory == null) return false;
                adapterHistory.getFilter().filter(newText);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                TransitionManager.beginDelayedTransition((ViewGroup) getActivity().findViewById(R.id.toolbar));

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onSuccess(JSONObject jsonObject) {
        Progress.hideLoading();
        JSONArray data = null;
        try {
            data = jsonObject.getJSONArray("data");
            List<History> list = this.getList(data);
            this.initRecycler(view, list);

        } catch (JSONException e) {

            new SnackBar().error(getContext(), "Error al parsear datos", (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
        }

    }


    private void getData(String filter, boolean isSwipe) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("filter", filter);
            jsonObject.put("id_user_create", User.id_user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(!isSwipe){

            Progress.showLoading((Objects.requireNonNull(getActivity())));
        }
        this.sendRequest(jsonObject, "sp_getTransactionQR");

    }


    public void onSuccessDelete(JSONObject jsonObject) {
        JSONArray data = null;
        try {
            data = jsonObject.getJSONArray("data");

            String statusResponse = data.getJSONObject(0).getString("Result");
            String message = data.getJSONObject(0).getString("Message");
            if (statusResponse.equals("Success")) {
                new SnackBar().success(getContext(), message ,(CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
                this.getData(currentFilter, false);

            } else {
                new SnackBar().error(getContext(), message, (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void onError(VolleyError error) {
        Progress.hideLoading();
        new SnackBar().error(getContext(), "Se ha producido un error", (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
    }

    private void sendRequest(JSONObject model, String sp) {
        try {

            RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("sp", sp);
            jsonBody.put("model", model);
            final String requestBody = jsonBody.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL_BASE + "execute", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        swipeLayout.setRefreshing(false);
                        onSuccess(new JSONObject(response));
                        //    closeDialog();
                    } catch (JSONException e) {
                        swipeLayout.setRefreshing(false);
                        new SnackBar().error(getContext(), "Error al obtener datos", (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    swipeLayout.setRefreshing(false);
                    onError(error);

                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendRequestDelete(JSONObject model, String sp) {
        try {
            Progress.showLoading((Objects.requireNonNull(getActivity())));
            RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("sp", sp);
            jsonBody.put("model", model);
            final String requestBody = jsonBody.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL_BASE + "execute", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Progress.hideLoading();
                        onSuccessDelete(new JSONObject(response));
                        //    closeDialog();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Progress.hideLoading();
                    onError(error);
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initSpinner(View view) {
        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.spinner_filter);
        spinner.setItems("Hoy", "Ayer", "Este mes");
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position == 0) {
                    currentFilter = "TODAY";
                    getData("TODAY", false);
                } else if (position == 1) {
                    currentFilter = "YESTERDAY";
                    getData("YESTERDAY", false);
                } else if (position == 2) {
                    currentFilter = "THIS_MONTH";
                    getData("THIS_MONTH", false);
                }
            }
        });
    }


    @SuppressLint("SimpleDateFormat")
    private List<History> getList(JSONArray jsonArray) throws JSONException {
        List<History> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            History history = new History();

            history.setAmount("Q " + object.getString("amount"));
            history.setDate(object.getString("date"));
            history.setDescription((object.getString("description").equals("null") ? "" : object.getString("description")));
            history.setId(object.getString("id"));
            history.setUsername(object.getString("username"));
            history.setId_user(object.getString("id_user"));
            history.setUser_type(object.getString("user_type"));
            history.setState(object.getString("state"));


            list.add(history);
        }
        return list;
    }

}
