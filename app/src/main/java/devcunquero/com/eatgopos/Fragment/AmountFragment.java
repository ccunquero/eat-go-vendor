package devcunquero.com.eatgopos.Fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

import devcunquero.com.eatgopos.Callback.ICloseDialog;
import devcunquero.com.eatgopos.Callback.ILoading;
import devcunquero.com.eatgopos.R;
import devcunquero.com.eatgopos.Util.Constant;
import devcunquero.com.eatgopos.Util.KeyBoard;
import devcunquero.com.eatgopos.Util.Progress;
import devcunquero.com.eatgopos.Util.SnackBar;
import devcunquero.com.eatgopos.Model.User;

public class AmountFragment extends DialogFragment {

    private EditText editAmount, editDescription;
    private ICloseDialog iCloseDialog;
    private Button btnSave;
    private ILoading iLoading;
    private String username, id, type;
    private AlertDialog alertDialog;
    private CoordinatorLayout CSnakbarLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_amount, container, false);

        TextView tvUsername = view.findViewById(R.id.tvAccount);
        TextView tvAvailable = view.findViewById(R.id.tvAvailableMoney);
        editAmount = view.findViewById(R.id.editAmount);
        editDescription = view.findViewById(R.id.editDescription);
        btnSave = view.findViewById(R.id.btn_save);
        CSnakbarLayout = (CoordinatorLayout) view.findViewById(R.id.containerMain);
        (view.findViewById(R.id.button_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();

            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }


        });
        Bundle bundle = getArguments();
        assert bundle != null;
        this.username = bundle.getString("USERNAME");
        this.id = bundle.getString("ID");
        this.type = bundle.getString("USER_TYPE");
        tvUsername.setText(this.username);
        tvAvailable.setText("Q " + bundle.getString("AVAILABLE_MONEY"));
        editAmount.requestFocus();

        InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                // On backpress, do your stuff here.
                Toast.makeText(getContext(), "hola mundo desde el backk press", Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.iCloseDialog = (ICloseDialog) context;


    }


    private void saveData() {
        JSONObject jsonObject = new JSONObject();
        try {

            if (!editAmount.getText().toString().equals("")) {

                jsonObject.put("amount", editAmount.getText().toString());
                jsonObject.put("description", editDescription.getText().toString());
                jsonObject.put("type", this.type);
                jsonObject.put("id_user", this.id);
                jsonObject.put("id_user_create", User.id_user);

                this.sendRequest(jsonObject, "sp_insTransactionQR");
            } else {
                editAmount.requestFocus();
                new SnackBar().error(getActivity(), "Ingrese un monto porfavor", CSnakbarLayout);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void onSuccess(JSONObject jsonObject) {
        JSONArray data = null;
        try {
            data = jsonObject.getJSONArray("data");

            String statusResponse = data.getJSONObject(0).getString("Result");
            String message = data.getJSONObject(0).getString("Message");
            if (statusResponse.equals("Success")) {
                new SnackBar().success(getContext(), message, (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
                this.closeDialog();
            } else {
                new SnackBar().error(getContext(), message, CSnakbarLayout);
            }


        } catch (JSONException e) {
            new SnackBar().error(getContext(), "Error al obtener datos", (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
        }

    }


    public void onErrorResponse(VolleyError error) {
        new SnackBar().error(getContext(), error.getMessage(), (CoordinatorLayout) getActivity().findViewById(R.id.containerMain));
    }

    private void sendRequest(JSONObject model, String sp) {
        try {
            Progress.showLoading(getActivity());
            RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("sp", sp);
            jsonBody.put("model", model);
            final String requestBody = jsonBody.toString();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.URL_BASE + "execute", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Progress.hideLoading();
                        onSuccess(new JSONObject(response));
                        //    closeDialog();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Progress.hideLoading();
                    AmountFragment.this.onErrorResponse(error);
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void closeDialog() {
        iCloseDialog.onCloseDialog();
        KeyBoard.hide(getContext(), editAmount);

        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                .remove(AmountFragment.this).commit();


    }


  

}