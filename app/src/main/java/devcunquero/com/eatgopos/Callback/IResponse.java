package devcunquero.com.eatgopos.Callback;

import android.support.v7.app.AppCompatActivity;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface IResponse {
    void onSuccess(JSONObject jsonObject, AppCompatActivity appCompatActivity);
    void onErrorResponse(VolleyError error, AppCompatActivity appCompatActivity);
}
