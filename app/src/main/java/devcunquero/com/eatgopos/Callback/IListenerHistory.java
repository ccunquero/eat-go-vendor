package devcunquero.com.eatgopos.Callback;


import devcunquero.com.eatgopos.Model.History;

/**
 * Created by Carlos on 25/09/2018.
 */

public interface IListenerHistory {
    void onItemDelete(History object, int position);

}
