package devcunquero.com.eatgopos.Callback;

public interface ILoading {
    void onInitLoading();
    void onFinishLoading();
}

