package devcunquero.com.eatgopos.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import devcunquero.com.eatgopos.Callback.IListenerHistory;
import devcunquero.com.eatgopos.Model.History;
import devcunquero.com.eatgopos.R;

public class RecyclerAdapterHistory extends RecyclerView.Adapter<RecyclerAdapterHistory.ViewHolder> implements Filterable {

    private Activity context;
    private List<History> historyList;
    private IListenerHistory listener;
    private List<History> fullList;


    public RecyclerAdapterHistory(Activity context, List<History> list, IListenerHistory listenerHistory) {
        this.context = context;
        this.historyList = list;
        this.listener = listenerHistory;
        this.fullList = new ArrayList<>(list);

    }

    @Override
    public RecyclerAdapterHistory.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_histroy, parent, false);
        RecyclerAdapterHistory.ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterHistory.ViewHolder holder, int position) {
        holder.onBind(historyList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id, date, description, amount, username;
        ImageButton btnMore;
        LinearLayout layoutApproved, layoutReversed;

        public ViewHolder(View itemView) {
            super(itemView);
            id = (TextView) itemView.findViewById(R.id.tvId);
            date = (TextView) itemView.findViewById(R.id.tvDate);
            description = (TextView) itemView.findViewById(R.id.tvDescription);
            amount = (TextView) itemView.findViewById(R.id.tvAmount);
            username = (TextView) itemView.findViewById(R.id.tvUsername);
            btnMore = (ImageButton) itemView.findViewById(R.id.btnMore);
            layoutApproved =  itemView.findViewById(R.id.containerApproved);
            layoutReversed = itemView.findViewById(R.id.containerReversed);

        }

        @SuppressLint("SetTextI18n")
        public void onBind(final History object, final IListenerHistory listener) {
            id.setText(String.valueOf(object.getId()));
            date.setText(object.getDate());
            description.setText(object.getDescription());
            amount.setText("Monto : " + object.getAmount());
            username.setText(object.getUsername());

            if(!object.getState().equals("APROBADO")){
                btnMore.setVisibility(View.GONE);
                layoutReversed.setVisibility(View.VISIBLE);
                layoutApproved.setVisibility(View.GONE);
            }else{
                btnMore.setVisibility(View.VISIBLE);
                layoutReversed.setVisibility(View.GONE);
                layoutApproved.setVisibility(View.VISIBLE);
            }

            if(object.getDescription().equals("")){
                description.setVisibility(View.GONE);
            }else{
                description.setVisibility(View.VISIBLE);
            }

            btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PopupMenu popup = new PopupMenu(context, btnMore);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.options_more);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.item_reverse:
                                    //handle menu1 click

                                    listener.onItemDelete(object, getAdapterPosition());


                                    return true;

                                default:
                                    return false;
                            }
                        }
                    });
                    //displaying the popup
                    popup.show();


                }
            });

        }
    }

    @Override
    public Filter getFilter() {
        return execFilter;
    }

    private Filter execFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<History> filter = new ArrayList<>();
            List<History> startWith = new ArrayList<>();
            List<History> contains = new ArrayList<>();

            if (charSequence == null || charSequence.length() == 0) {
                filter.addAll(fullList);
            } else {
                String search = charSequence.toString().toLowerCase().trim();
                for (History object : fullList) {

                    String filterPattern = object.getUsername().toLowerCase().trim();
                    if (filterPattern.startsWith(search)) {
                        startWith.add(object);
                    } else if (filterPattern.contains(search)) {
                        contains.add(object);
                    } else if (object.getId().startsWith(search)) {
                        startWith.add(object);
                    } else if (object.getId().contains(search)) {
                        contains.add(object);
                    }
                }
                filter.addAll(startWith);
                filter.addAll(contains);
            }
            FilterResults results = new FilterResults();
            results.values = filter;

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            historyList.clear();
            historyList.addAll((List) filterResults.values);
            notifyDataSetChanged();
        }
    };


}
